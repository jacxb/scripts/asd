#!/bin/bash

# Funcs
failout() {
	[ -z "$MSG" ] || echo $MSG
	cat << EOF
Usage: $(basename $0) command_group command extra_inputs
EOF
	exit 11
}

get_config_value() {
	#cat $INSTALL_DIR/config | grep "^$1" | cut -d'=' -f2 | head -n1
	cat $INSTALL_DIR/config | grep "^$1" | head -n1 | sed -E "s/^[0-9A-Z_-]+=//"
}

config_compile_regions() {
	AWS_PROFILE_SOMETHING=$(get_config_value AWS_PROFILE_SOMETHING)
}

# Get input vars
r=0
p=0
d=0
while getopts ":r:p:d" o; do
	case "${o}" in
	r)
		r=${OPTARG}
		;;
	p)
		p=${OPTARG}
		;;
	d)
		d=1
		;;
	*)
		MSG="Invalid option detected" failout
		;;
	esac
done
shift $((OPTIND-1))

# Build Variables
INSTALL_DIR=$(dirname $0)
[ "$r" == 0 ] || AWS_REGION="$r"
[ -z "${AWS_REGION}"  ] && AWS_REGION=$(cat $INSTALL_DIR/config | grep "^DEFAULT_AWS_REGION" | cut -d'=' -f2)
if [ -z "${AWS_PROFILE}" ] ; then
	config_compile_regions

	# Assign inputs
	case "${p}" in
		something)
			AWS_PROFILE="$AWS_PROFILE_SOMETHING"
			;;
		*)
			AWS_PROFILE="${p}"
			;;
	esac
	
	[ -z "$AWS_PROFILE" ] && AWS_PROFILE="$AWS_PROFILE_LIVE"
	[ "$AWS_PROFILE" == 0 ] && AWS_PROFILE="$AWS_PROFILE_LIVE"
fi

JIRA_DOMAIN=$(get_config_value JIRA_DOMAIN)
JIRA_EMAIL=$(get_config_value JIRA_EMAIL)
JIRA_API_TOKEN=$(get_config_value JIRA_API_TOKEN)

# Find script to run
# Recursively look through dirs for command
COMMAND=$(find $INSTALL_DIR/f -type d -maxdepth 1 -iname "$1*" | head -n1)
[ -z "$COMMAND" ] && MSG="Didnt find a command group ($1)" failout

while [ -d $COMMAND ]
do
	shift 1
	COMMAND_COUNT=$(find $COMMAND -maxdepth 1 -iname "$1*" | wc -l)
	[ "$COMMAND_COUNT" -gt 1 ] && MSG="Multiple options found for $1" failout
	COMMAND=$(find $COMMAND -maxdepth 1 -iname "$1*")
done

shift 1

if [ "$d" == 1 ] ; then
	cat << EOF
\$AWS_PROFILE    = "$AWS_PROFILE"
\$AWS_REGION     = "$AWS_REGION"
\$JIRA_DOMAIN    = "$JIRA_DOMAIN"
\$JIRA_EMAIL     = "$JIRA_EMAIL"
\$JIRA_API_TOKEN = "$JIRA_API_TOKEN"
EOF
fi

JIRA_DOMAIN="${JIRA_DOMAIN}" \
	JIRA_EMAIL="${JIRA_EMAIL}" \
	JIRA_API_TOKEN="${JIRA_API_TOKEN}" \
	INSTALL_DIR="${INSTALL_DIR}" \
	AWS_PROFILE="${AWS_PROFILE}" \
	AWS_REGION="${AWS_REGION}" \
	exec $COMMAND $@
